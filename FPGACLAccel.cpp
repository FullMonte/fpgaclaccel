#include "FPGACLAccel.h"
#include "FullMonteFPGACLTypes.h"
#include "FullMonteFPGACLConstants.h"

#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <ctime>

using namespace std;
using namespace FPGACL;
using namespace aocl_utils;

////////////////////////////////////////////////////////////////////////////////////////
// enum for each kernel type
enum Kernels {
    RNGINIT_KERN = 0,
    RNG_KERN,
    LAUNCH_KERN,
    DRAWSTEP_KERN,
    HOP_KERN,
    INTERFACE_KERN,
    DROPSPIN_KERN,
    EXIT_KERN,
    NUM_KERNS
};

// name of the different kernels in FullMonte kernel (corresponds to enum above)
const char* const kernel_name[NUM_KERNS] = {
    "rng_init_kernel",
    "rng_kernel",
    "launch_kernel",
    "draw_step_kernel",
    "hop_kernel",
    "interface_kernel",
    "drop_spin_kernel",
    "exit_kernel"
};

// number of write events to the FPGA (materials, tetras and launch packets)
const unsigned      NUM_WRITE_EVENTS = 3;

// global OpenCL related stuff
cl_platform_id      platform = NULL;

cl_context          context = NULL;
cl_program          program = NULL;

cl_device_id        device_id;

// kernel variables
cl_command_queue    queue[NUM_KERNS];
cl_kernel           kernel[NUM_KERNS];
cl_event            kernel_event[NUM_KERNS];
cl_event            write_event[NUM_WRITE_EVENTS];

// variables related to FullMonte
cl_uint             seed_d;
cl_float            wmin_d, prwin_d;

cl_uint             num_materials_d;
cl_mem              materials_d;

cl_uint             num_tetras_d;
cl_mem              tetras_d;
    
cl_ulong            num_packets_d ;
cl_mem              launched_packets_d;

cl_mem              tetra_energy_d;

/**
 * Cleanup memory allocated for OpenCL
 */
void cleanup() {
    cout << "FPGACL: Cleaning up OpenCL memory\n";

    // release the queue and kernel object for each kernel
    for(unsigned k = 0; k < NUM_KERNS; k++) {
        if(queue[k]) {
            clReleaseCommandQueue(queue[k]);
        }

        if(kernel[k]) {
            clReleaseKernel(kernel[k]);
        }
    }

    // release materials
    if(materials_d) {
       clReleaseMemObject(materials_d); 
    }
    
    // release launched_packets
    if(launched_packets_d) {
       clReleaseMemObject(launched_packets_d); 
    }
    
    // release tetras
    if(tetras_d) {
       clReleaseMemObject(tetras_d); 
    }
    
    // release tetra fluences
    if(tetra_energy_d) {
       clReleaseMemObject(tetra_energy_d); 
    }
    
    // release program
    if(program) {
        clReleaseProgram(program);
    }
    
    // release context
    if(context) {
        clReleaseContext(context);
    }

    cout << "FPGACL: Done OpenCL cleanup\n";

}
////////////////////////////////////////////////////////////////////////////////////////

/**
 * Initialize the device
 *
 * @param aocx_binary_file the AOCX binary file to use to program the FPGA
 * @param seed the random seed for the RNG
 * @param wmin the minimum weight before a packet goes to roulette
 * @param prwin the percentage of a roulette win
 * @param materials the list of materials
 * @param tetras the mesh tetras
 * @returns true on success, false on failure
 */
bool FPGACL::init(
    const string                        aocx_binary_file,
    const unsigned                      seed,
    const float                         wmin,
    const float                         prwin,
    const vector<FPGACL::Material>&     materials,
    const vector<FPGACL::Tetra>&        tetras
)
{
    cl_int status;
    
    //// first initialize the global stuff to do with Intel FPGA OpenCL
    cout << "FPGACL: Initializing OpenCL\n";

    // move to CWD of exe
    if(!setCwdToExeDir()) {
        return false;
    }

    // get the OpenCL platform.
    platform = findPlatform("Intel(R) FPGA SDK for OpenCL(TM)");
    if(platform == NULL) {
        cout << "FGPACL: Unable to find Intel(R) FPGA OpenCL platform.\n";
        return false;
    }

    // query the available OpenCL devices.
    scoped_array<cl_device_id> devices;
    unsigned num_devices = 0;

    devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));
    cout << "FPGACL: Platform " << getPlatformName(platform) << endl;
    cout << "FPGACL: Found " << num_devices << " devices\n";
    for(unsigned i = 0; i < num_devices; ++i) {
        cout << "\t" << getDeviceName(devices[i]) << endl;
    }

    // TODO: allow user to select device?
    if(num_devices > 0) {
        cout << "FPGACL: By default using device 0: " << getDeviceName(devices[0]) << endl;
        device_id = devices[0];
    } else {
        cerr << "FPGACL: Found 0 devices.\n";
        return false;
    }

    // Create the context.
    context = clCreateContext(
        NULL,
        1,
        &device_id,
        &oclContextCallback,
        NULL,
        &status
    );
    checkError(status, "Failed to create context");

    // make sure we can find the AOCX
    if(!aocl_utils::fileExists(aocx_binary_file.c_str())) {
        cerr << "FPGACL: AOCX file: " << aocx_binary_file << " does not exist" << endl;
        return false;
    } else {
        cout << "FPGACL: Using AOCX: " << aocx_binary_file << endl;
    }

    // create the program from the AOCX binary file
    program = createProgramFromBinary(
        context,
        aocx_binary_file.c_str(),
        &device_id,
        1
    );

    //  build the program that was just created.
    status = clBuildProgram(
        program,
        0,
        NULL,
        "",
        NULL,
        NULL
    );
    checkError(status, "Failed to build program");

    //// Global initialization done, do stuff specific to our kernel

    if(materials.size() > MAX_FPGACL_MATERIALS) {
        fprintf(stderr, "FPGACL: the number of materials (%lu) exceeds the maximum (%d) of FPGACL\n", materials.size(), MAX_FPGACL_MATERIALS);
        return false;
    }

    // NOTE: this assumes max number of tetras is how many we can fit on-chip
    // change this once we use global memory for tetras in kernel
    if(tetras.size() > MAX_FPGACL_ONCHIP_TETRAS) {
        fprintf(stderr, "FPGACL: the number of tetras (%lu) exceeds the maximum number of on-chip tetras (%d) of FPGACL\n", tetras.size(), MAX_FPGACL_ONCHIP_TETRAS);
        return false;
    }

    // copy scalar args
    seed_d = seed;
    wmin_d = wmin;
    prwin_d = prwin;
    num_materials_d = materials.size();
    num_tetras_d = tetras.size();

    cout << "FPGACL: Setting up kernels\n";

    // create each kernel and the its command queue
    for(unsigned k = 0; k < NUM_KERNS; k++) {
        // command queue.
        queue[k] = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &status);
        checkError(status, "Failed to create command queue for kernel '%s'", kernel_name[k]);

        // kernel.
        kernel[k] = clCreateKernel(program, kernel_name[k], &status);
        checkError(status, "Failed to create kernel '%s'", kernel_name[k]);
    }

    //// create global memory inputs
    // materials
    materials_d = clCreateBuffer(
        context,
        CL_MEM_READ_ONLY,
        num_materials_d * sizeof(Material),
        NULL,
        &status
    );
    checkError(status, "Failed to create buffer for Materials");

    // tetras
    tetras_d = clCreateBuffer(
        context,
        CL_MEM_READ_ONLY,
        num_tetras_d * sizeof(Tetra),
        NULL,
        &status
    );
    checkError(status, "Failed to create buffer for Tetras");

    //// create global memory outputs
    // tetra volume energy
    tetra_energy_d = clCreateBuffer(
        context,
        CL_MEM_READ_WRITE,
        num_tetras_d * sizeof(cl_long),
        NULL,
        &status
    );
    checkError(status, "Failed to create buffer for tetra energy");

    // initialize tetra energy to 0
    cl_long init_val = 0.0;
    status = clEnqueueFillBuffer(
        queue[LAUNCH_KERN],
        tetra_energy_d,
        &init_val,
        sizeof(cl_long),
        0,
        num_tetras_d * sizeof(cl_long),
        0,
        NULL,
        NULL
    );
    checkError(status, "Failed to initialize tetra energy to 0\n");

    // copy the materials to the FPGA
    status = clEnqueueWriteBuffer(
        queue[LAUNCH_KERN],
        materials_d,
        CL_FALSE,
        0,
        num_materials_d * sizeof(Material),
        &materials[0],
        0,
        NULL,
        &write_event[0]
    );
    checkError(status, "Failed to transfer Material data");

    // copy the tetras to the FPGA
    status = clEnqueueWriteBuffer(
        queue[LAUNCH_KERN],
        tetras_d,
        CL_FALSE,
        0,
        num_tetras_d * sizeof(Tetra),
        &tetras[0],
        0,
        NULL,
        &write_event[1]
    );
    checkError(status, "Failed to transfer Tetra data");

    cout << "FPGACL: Global memory allocated and initialized.\n";

    return true;
}

/**
 * Launches the simulation (async) by:
 *  1) Creating and initializing OpenCL input/output buffers
 *  2) Set the kernel arguments
 *  3) Launching the kernels
 *
 * If you want to wait for the kernel use CUDAAccel::sync()
 *
 * @param launched_packets the packets already launched by an emitter (in host)
 */
bool FPGACL::launch(const vector<FPGACL::LaunchPacket>& launched_packets) {
    cl_int status;
    
    cout << "FPGACL: Setting kernel arguments\n";
    
    // how many packets?
    num_packets_d = launched_packets.size();

    //// create global memory inputs
    // pre-launched packets
    launched_packets_d = clCreateBuffer(
        context,
        CL_MEM_READ_ONLY,
        num_packets_d * sizeof(LaunchPacket),
        NULL,
        &status
    );
    checkError(status, "Failed to create buffer for LaunchPackets");

    // copy the launched packets to the FPGA
    status = clEnqueueWriteBuffer(
        queue[LAUNCH_KERN],
        launched_packets_d,
        CL_FALSE,
        0,
        num_packets_d * sizeof(LaunchPacket),
        &launched_packets[0],
        0,
        NULL,
        &write_event[2]
    );
    checkError(status, "Failed to transfer LaunchPacket data");
    
    // set kernel arguments
    for(unsigned k = 0; k < NUM_KERNS; k++) {
        // the current argument index for the kernel
        unsigned argi = 0;

        // set different kernel arguments based on the current kernel
        switch(k) {
            // RNGINIT
            case RNGINIT_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &seed_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                break;

            // RNG
            case RNG_KERN:
                break;

            // LAUNCH
            case LAUNCH_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_ulong), &num_packets_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &launched_packets_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                break;

            // DRAWSTEP
            case DRAWSTEP_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_tetras_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &tetras_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                break;
            
            // INTERFACE
            case INTERFACE_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_tetras_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &tetras_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                break;

            // HOP
            case HOP_KERN:
                break;

            // DROPSPIN
            case DROPSPIN_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_float), &wmin_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_float), &prwin_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_uint), &num_tetras_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &materials_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);

                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_mem), &tetra_energy_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                break;

            // EXIT
            case EXIT_KERN:
                status = clSetKernelArg(kernel[k], argi++, sizeof(cl_ulong), &num_packets_d);
                checkError(status, "Failed to set argument %d for kernel '%s'", argi - 1, kernel_name[k]);
                break;

            // UNKNOWN: SHOULD NOT HAPPEN
            default:
                cerr << "FPGACL: Unknown kernel enum " << k << endl;
                return -1;
        }
    }

    cout << "FPGACL: Kernel arguments set. Launching kernels...\n";

    // launch the kernels
    for(unsigned k = 0; k < NUM_KERNS; k++) {
        status = clEnqueueTask(
            queue[k],
            kernel[k],
            NUM_WRITE_EVENTS,
            write_event,
            &kernel_event[k]
        );
        checkError(status, "Failed to launch '%s' kernel", kernel_name[k]);
    }

    cout << "FPGACL: Kernels launched\n";

    // release write events
    for(unsigned i = 0; i < NUM_WRITE_EVENTS; i++) {
        clReleaseEvent(write_event[i]);
    }

    return true;
}

/**
 * Wait for the kernel(s) to finish.
 * Should be called when you want to wait for FPGACL::launch to finish
 */
//clGetProfileInfoIntelFPGA(cl_event);

void FPGACL::sync() {
    // wait for specific events
    // EXIT finishing tells us when all packets have exited the pipeline
    // DROPSPIN finishing tells us that all absorption events have been pushed back to global memory
    cout << "FPGACL: Waiting on finish event from kernels...\n";
    clWaitForEvents(1, &kernel_event[EXIT_KERN]);
    clWaitForEvents(1, &kernel_event[DROPSPIN_KERN]);
}

/**
 * Cleanup allocated memory.
 */
void FPGACL::destroy() {
    cleanup();
}

/**
 * Copies the energy of the tetras computed by the device into a host array.
 * NOTE: You MUST call CUDAAccel::sync before this or you will have some problems!
 *
 * @param tetraEnergy the host tetra energies to copy into.
 */
void FPGACL::copyTetraEnergy(vector<double>& tetraEnergy) {
    cout << "FPGACL: Reading results from device\n";

    // temporary array for fixed point accumulations
    vector<long> tetraEnergyFixed(num_tetras_d);

    // read the fluences from device back to host
    cl_int status;
    status = clEnqueueReadBuffer(
        queue[EXIT_KERN],
        tetra_energy_d,
        CL_TRUE,
        0,
        num_tetras_d * sizeof(long),
        &tetraEnergyFixed[0],
        0,
        NULL,
        NULL
    );
    checkError(status, "Failed to read tetra fluences from device");

    // ensure the output array has the correct size
    tetraEnergy.resize(num_tetras_d);
    
    // convert from fixed to double for output
    for(unsigned i = 0; i < num_tetras_d; i++) {
        tetraEnergy[i] = FIXED2DOUBLE(tetraEnergyFixed[i]);
    }
}

/**
 * Copies the energy of the tetras computed by the device into a host array.
 * The host may use floats after the absorption is done so this is for convenience.
 * See FPGACLAccel::copyTetraEnergy(<float>)
 *
 * @param tetraEnergy the host tetra energies to copy into.
 */
void FPGACL::copyTetraEnergy(vector<float>& tetraEnergy) {
    // copy doubles from device
    vector<double> tmp(num_tetras_d);
    copyTetraEnergy(tmp);

    // convert to floats
    for(unsigned t = 0; t < num_tetras_d; t++) {
        tetraEnergy[t] = tmp[t];
    }
}

