# Introduction
FullMonte is a flexible and accurate simulator for light propagation in complex media, such as tissue. FPGACLAccel is an OpenCL implementation of the FullMonte simulator that uses the Intel FPGA SDK for OpenCL to target Intel FPGA's.

# Dependencies
- Intel FPGA OpenCL SDK 17.0-18.1 (could support ealier versions with define statements to substitute 'altera' for 'intel'). This includes Quartus pro.

# Directory Structure
the root directory contains the following directories and files:
- **FPGAAccel.{cpp/h}**: The API for using the accelerator from the host.
- **FullMonteFPGACLAccelConstants.h**: FullMonte constants particular for the FPGACL accelerator.
- **FullMonteCUDATypes.h**: The datatypes (Tetra, Material, Packet, etc) used inside the CUDA kernels. Needed by host and device. It is up to the host (FullMonteSW) to make sure these datatypes are set up correctly.
- **CMakeLists.txt**: The CMake file for the library (used by FullMonteSW for building API)
- **opencl_util/** : The utility functions provided by Intel for the OpenCL SDK.
- **README.md** : This file
- **kernels/**: All the Intel OpenCL SDK kernels (described in more detail in the next section).

# kernels
- **channels.h**: Header file for all channel related data structures for the pipeline.
- **intersection.h**: Shared fuction `getIntersection()` defined in here because it is used by more than one of the kernel files below.
- **draw_step.cl**: DRAW STEP kernel.
- **drop_spin.cl**: DROP SPIN kernel.
- **exit.cl**: EXIT kernel.
- **hop.cl**: HOP kernel.
- **interface.cl**: INTERFACE kernel.
- **launch.cl**: LAUNCH kernel.
- **rng.cl**: RANDOM NUMBER GENERATOR (RNG) kernel.
- **full_monte_device.cl**: The main kernel file. Combines all kernels above into a single file. This is the file built by `aoc`.
- **Makefile** : Makefile for the device, described in the next section.

# Building the Device (FPGA)
**NOTE:** Building the *device* (i.e. the AOCX binary file to program the FPGA) is **NOT**  included in the regular build flow of FullMonte. This is due to the fact that a device build can take ~hours and we don't want to accidently explode the build time. In general you should build the AOCX using the instructions below (either for the actual device or the emulator) and once an AOCX is generated you can point to it using the TetraMCFPGACLKernel `binaryFile` function.<br/>

The devices Makefile is well commented and should be read for more information on what is exactly happening.
The device has a few build targets defined in the Makefile
- **quick**: This uses the `aoc` *-c* option which simply runs the parser to check for validity. Use this to check for compile errors/warnings. **~seconds**
- **report**: This generates the static analysis report in *FullMonteCL_Root*/device/build/full_monte_device/reports for the device pointed to by the `AOCL_BOARD_PACKAGE_ROOT` environment variable. This directory contains the report HTML file. **~minutes (~1 hour)**
- **emulate**: This generates the shared libraries for the emulator in *FullMonteCL_Root*/device/build/full_monte_device/ and an AOCX file for the emulator. **~seconds**
- **all** (or simply `make`): This builds the AOCX for the actual FPGA. It runs Quartus after generating the Verilog to create a programmable file. This command will generate reports the same as the *report* command but will include more accurate information and timing information (i.e. Fmax). **~hours**
- **de10pro**: Same as *all* but adds the argument `-board=s10_gl2e1` to build the kernel for the DE10-Pro Terasic board (installed in SAVI2). **~hours**

To build for emulation do the following:
- `cd` to the `kernels/`
- run `make emulation` (you may have to run `make clean` first)
- Ensure `CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1` environment variable is set to tell the runtime to use the emulator (you cannot have this set if you want to target an actual board)

To build for actual hardware do the following (Assuming `AOCL_BOARD_PACKAGE_ROOT` is set correctly):
- `cd` to `kernels/`
- run `make` (you may have to run `make clean` first)
- *wait roughly 8 hours :/*
-

To generate reports (static analysis and resource estimations) do the following:
- `cd` to `kernels/`
- run `make report` (you may have to run `make clean` first)
- The report is in `kernels/Build/full_monte_device/reports` (the entire *reports* directory is required to view it!).

Section 7 in the [Intel OpenCL SDK programming guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/opencl-sdk/aocl_programming_guide.pdf) contains more information on the different build options and what they do.

# Useful resources
[Intel OpenCL SDK getting started guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/opencl-sdk/aocl_getting_started.pdf)<br />
[Intel OpenCL SDK programming guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/opencl-sdk/aocl_programming_guide.pdf)<br />
[Intel OpenCL SDK best practices guide](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/opencl-sdk/aocl-best-practices-guide.pdf)<br />
[Terasic DE10-Pro Webpage](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=13&No=1144&PartNo=4)<br />
[Terasic DE5a-Net-DDR4 Webpage](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=231&No=1108)<br />
[FullMonteSW](https://gitlab.com/FullMonte/FullMonteSW)<br />

# Trouble shooting
## FPGA USB Permissions
Sometimes the permissions of the FPGA USB can be incorrect which will not allow you to program the FPGA. To fix this do the following:
1.  run `dmesg | grep usb` (example output below) and locate the USB with manufacturer *Altera*
```
...
[ 11.920040] usb 3-8: new high-speed USB device number 2 using xhci_hcd
[ 12.116403] usb 3-8: New USB device found, idVendor=09fb, idProduct=6010
[ 12.116405] usb 3-8: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[ 12.116405] usb 3-8: Product: DE10-Pro
[ 12.116406] usb 3-8: Manufacturer: Altera
[ 12.116407] usb 3-8: SerialNumber: DE10-0001-00001
...
```

2.  run a command like: `sudo chmod 666 /dev/bus/usb/003/002` where `003` is for usb 3-8 and `002` is for device number 2
3.  check that the permission change suceeded by listing the availible devices in Quartus using the commmand `quartus_pgm -l`

## FPGA programming errors with OpenCL AOCX files
Sometimes the Intel OpenCL SDK runtime will fail to program the AOCX file and complain about not finding a board. First, try running `aocl diagnose` and check if it fails or succeeds. If this does not work, you may have to *bringup* the board again. To do so, `cd` to the directory with the BSP bringup script, for example `cd $AOCL_BOARD_PACKAGE_ROOT`, then run `./bringup.sh fpga` to program the OpenCL shell on the FPGA.
Once this has finished, restart the machine using the command `sudo /sbin/shutdown -r now`. Once the machine is rebooted, try running `aocl diagnose` again to see the status of the board. These instructions assumed that the board has already been installed using the board providers instructions (e.g. for the [DE10-Pro board](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=13&No=1144&PartNo=4)).
<br />
**NOTE:** in the case where the board is being used both by OpenCL developers and *regular* FPGA developers (i.e. using Quartus), anytime the FPGA is reprogrammed using an SOF, the OpenCL developer will have to rerun the bringup script to program the FPGA with the OpenCL `top.sof` file.

