#ifndef _FULLMONTEFPGACLTYPES_H_
#define _FULLMONTEFPGACLTYPES_H_

//
// This file is used to typedef types used
// in both the host and device.
//

// OpenCL has types defined, for use by the host,
// in opencl.h. These types have a cl_* prefix.
// In order to use these types in BOTH the host and
// device we typedef the cl_* variables to the same
// typename minus the prefix (e.g. cl_float3 -> float3).
// In the device, the types do NOT have the cl_* prefix
// so we ONLY typedef these variables if we are building
// host (i.e. FULLMONTE_HOST_BUILD is defined). That way
// the datatypes defined in host_device/Datatypes can be
// shared between the host and device
#ifdef FPGACL_HOST_BUILD
    #include "CL/opencl.h"

    typedef cl_uchar    uchar;

    typedef cl_ushort   ushort;

    typedef cl_uint2    uint2;
    typedef cl_uint3    uint3;
    typedef cl_uint4    uint4;

    typedef cl_int2     int2;
    typedef cl_int3     int3;
    typedef cl_int4     int4;

    typedef cl_float2   float2;
    typedef cl_float3   float3;
    typedef cl_float4   float4;
#endif

// OpenCL doesn't like namespaces ...
#ifdef FPGACL_HOST_BUILD
namespace FPGACL {
#endif

/**
 * Tetrahedron representation
 * Normals (nx, ny, nz) are oriented so that points inside 
 * the tetra have a positive altitude (h=dot(p,n)-C)
 */
typedef struct {
    // x component of normal vector for all 4 tetra faces
    float4           nx;

    // y component of normal vector for all 4 tetra faces
    float4           ny;

    // z component of normal vector for all 4 tetra faces
    float4           nz;

    // Constants of all 4 tetra faces. Used to check if a point is inside the Tetra
    float4           C;

    // The four directed face IDs of the current tetra extracted from the array of faces
    uint4            IDfds;

    // Adjacent tetras ID for each face of the current tetra
    uint4            adjTetras;

    // Material ID for this tetra
    uchar            matID;

    // Flags for each face (4Byte = 32Bit -> 8Bit each)
    // For face i=0..3, content of (faceFlags >> (i<<3)) is:
    // Bit  Meaning
    // ===  =======
    // 7
    // 6
    // 5
    // 4
    // 3
    // 2
    // 1
    // 0    whether to score fluence through the surface (DirectedSurfaceScorer)
    //
    // NOTE: not used, yet...
    //unsigned int    faceFlags;
} __attribute__((aligned(128))) Tetra;

/**
 * Represents a material in the mesh
 * TODO: Lots of repeated information here
 */
typedef struct {
    // scattering coefficient
    float               mu_s;  // not used

    // absorbing coefficient
    float               mu_a;  // not used

    // g in different forms
    float               g;
    float               gg;
    float               recip_2g;
    float               one_minus_gg;
    float               one_plus_gg;

    // refractive index
    float               n;

    // propagation vector along unit physical step: time elapsed increases,
    // dimensionless step decreases by mu_t, physical step decreases by 1
    float3              m_prop;

    // initial propogation vector
    // (1/mu_t physical step remaining, 1 dimensionless step remaining, 0 time)
    float3              m_init;
    
    // mu_a + mu_s
    float               mu_t;
    float               recip_mu_t;

    // fraction absorbed at each interaction: mu_a / mu_t
    float               absfrac;

    // does the material scatter: mu_s != 0
    bool                scatters;

    // whether the material is isotropic (g ~= 0.0)
    bool                isotropic;
} __attribute__((aligned(128))) Material;

/**
 * Represents the result of a step
 */
typedef struct {
    // Position (x, y, z) of the next position of the packet
    float3          Pe;

    // Distance to the nearest face of the tetra where the packet is in
    // or the random step length for the packet
    float           distance;

    // Face ID of the tetras face which has the smallest distance to the
    // packets current position.
    int             IDfe;

    // Tetra ID of the tetra which is adjacent to the face which is nearest
    // to the packets current position (IDfe)
    uint            IDte;

    // The index of the nearest face to the packet
    int             idx;

    // Boolean value: True if the packet intersects with the face of the current
    // tetra within the random step length. I.e. the packet leaves the current tetra
    bool            hit;
} StepResult;

/**
 * The direction for a packet
 */
typedef struct {
    // Direction vector
    float3              d;

    // First unit auxilary vector orthonormal to the direction of travel d and b
    float3              a;
    
    // Second unit auxilary vector orthonormal to the direction of travel d and a
    float3              b;
} PacketDirection;

/**
 * The structure for a Packet
 * These are passed by channels between kernels
 */
typedef struct {
    // The current direction of the packet
    PacketDirection     dir;

    // Position of the packet [x, y, z]
    float3              p;
    
    // Dimensionless step length remaining
    // [physical distance, Mean Free Paths (MFPs) remaining to go, time]
    float3              s;
    
    // Current packet weight
    float               w;

    // the current tetraID the packet is in
    uint                tetraID;
} Packet;

/**
 * A packet specifically at the moment of launch
 * It has no step length (not drawn yet) or weight (implicitly 0)
 */
typedef struct {
    // The current direction of the packet
    PacketDirection     dir;

    // Position of the packet [x, y, z]
    float3              p;

    // The tetra ID
    uint                tetraID;    
} __attribute__((aligned(64))) LaunchPacket;

#ifdef FPGACL_HOST_BUILD
}  // END: FPGACL namespace
#endif

#endif


