# the AOC compiler
AOC=aoc

# AOCX target name and directory
TARGET=full_monte_device
TARGET_DIR=./Build

# files
INCS := $(wildcard ./*.h)
SRCS := $(wildcard ./*.cl)
MAIN_SRC = $(TARGET).cl

# include directories
INC_DIRS = . ..

# these flags are present for all builds of the device
AOCFLAGS= -DKERNEL_PROGRAM

######################################################################################
## Various targets
all: $(TARGET_DIR)/$(TARGET)

quick: AOCFLAGS += -Wall -c
quick: all

report: AOCFLAGS += -rtl -report
report: all

emulate: AOCFLAGS += -march=emulator -emulator-channel-depth-model=default -DEMULATION
emulate: all

de10pro: AOCFLAGS += -board=s10_gl2e1
de10pro: all

de10pro_profile: AOCFLAGS += -profile=all
de10pro_profile: de10pro
######################################################################################

# target build
$(TARGET_DIR)/$(TARGET) : Makefile $(SRCS) $(INCS)
	$(AOC) $(AOCFLAGS) $(foreach D,$(INC_DIRS),-I$D) $(MAIN_SRC) -o $(TARGET_DIR)/$(TARGET)

# clean
clean: remove
clean:
	rm -f $(TARGET_DIR)/$(TARGET).*
	rm -rf $(TARGET_DIR)/$(TARGET)

# remove
remove:
	rm -rf $(TARGET_DIR)/*temp
	rm -f $(TARGET_DIR)/$(TARGET).aoco $(TARGET_DIR)/$(TARGET).aocr

.PHONY: all quick report emulate profile remove clean
