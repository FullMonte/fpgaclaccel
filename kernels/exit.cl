#include "channels.h"

/**
 * EXIT kernel
 * gathers packets that have exited either by roulette or exiting mesh
 *
 * @param num_packets the number of packets to simulate
 */
__kernel void exit_kernel(const ulong num_packets)
{
    uint packets_remaining = (uint) num_packets;

    #ifdef EMULATION
    ulong last_shown = 0;
    #endif
    
    while(packets_remaining != 0) {
        // Try reading channels in parallel
        bool valid_dropspin = false, valid_interface = false;
        (void) read_channel_nb_intel(DROPSPIN_TO_EXIT, &valid_dropspin);
        (void) read_channel_nb_intel(INTERFACE_TO_EXIT, &valid_interface);

        // reduce number of packets remaining
        if(valid_dropspin) {
            packets_remaining--;
        }
        if(valid_interface) {
            packets_remaining--;
        }
        
        #ifdef EMULATION
        // NOTE: this is only for display, no kernel logic here
        // in emulation, show how many steps remain.
        if((packets_remaining != last_shown) && (packets_remaining % 10000 == 0)) {
            printf("DEVICE (EXIT): %d packets remaining\n", packets_remaining);
            last_shown = packets_remaining;
        }
        #endif
    }

    #ifdef EMULATION
    printf("DEVICE (EXIT): Finished processing %lu packets\n", num_packets);
    #endif

    // tell DROPSPIN tha the simulation is over
    // which signals it to write back its memory
    write_channel_intel(EXIT_TO_DROPSPIN, true);
}

