#include "channels.h"

#include "FullMonteFPGACLConstants.h"
#include "FullMonteFPGACLTypes.h"

/**
 * LAUNCH kernel
 * launches LaunchPackets into the channel to be grabbed by the STEP kernel
 *
 * @param num_packets the number of packets to launch
 * @param launched_packets the array of packets launched by the host
 */
__kernel void launch_kernel(
    const ulong                         num_packets,
    __constant const LaunchPacket*      launched_packets
) 
{
    // packet launching loop
    for(unsigned int i = 0; i < num_packets; i++) {
        // get the packet to launch from global memory (setup by host)
        LaunchPacket lpkt = launched_packets[i];
        
        // create a Packet from the LaunchPacket
        Packet pkt = {
            .dir = lpkt.dir,
            .p = lpkt.p,
            .s = (float3)(0.0f, 0.0f, 0.0f),
            .w = 1.0f,
            .tetraID = lpkt.tetraID
        };

        // create data to be pushed to the channel
        ChannelData toDrawstep = {
            .pkt = pkt,
            //.stepResult = Do not care
            //.tetra = Do not care,
            .state = DRAWSTEP
        };

        // launch a new packet into the pipeline using a blocking
        // call which stalls the kernel here until a pipeline bubble
        // allows a new packet to enter the pipeline
        write_channel_intel(LAUNCH_TO_DRAWSTEP, toDrawstep);
    }

    #ifdef EMULATION
    printf("DEVICE (LAUNCH): Done launching %lu packets\n", num_packets);
    #endif
}

