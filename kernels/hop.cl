#include "channels.h"

#include "FullMonteFPGACLTypes.h"

/**
 * HOP kernel
 *
 * @param materials_mem the pointer to the list of materials.
 */
__kernel void hop_kernel()
{
    while(true) {
        bool valid = false;
        ChannelData chanData = read_channel_nb_intel(DRAWSTEP_TO_HOP, &valid);
        
        if(valid) {
            if(chanData.state == HOP) {
                // pull the info from the channel data
                Packet pkt = chanData.pkt;
                const Tetra currTetra = chanData.tetra;

                // TODO: check for stepResult.idx > 3. See FullMonteSW

                // compute step result, this moves the packet and checks for a tetra intersection
                const StepResult stepResult = getIntersection(&currTetra, &pkt);
            
                // update the position of the packet p' = p + s*d
                pkt.p = stepResult.Pe;

                // move to INTERFACE kernel but mark state as INTERFACE or DROPSPIN based on StepResult
                const ChannelData interfaceData = {
                    .pkt = pkt,
                    .stepResult = stepResult,
                    .tetra = currTetra,
                    .state = stepResult.hit ? INTERFACE : DROPSPIN
                };
                
                chanData = interfaceData;
            }
            
            write_channel_intel(HOP_TO_INTERFACE, chanData);
        }
    }
}

