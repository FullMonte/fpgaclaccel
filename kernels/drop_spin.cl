#include "channels.h"

#include "FullMonteFPGACLConstants.h"
#include "FullMonteFPGACLTypes.h"

/**
 * enum for roulette outcomes
 * only used internally
 */
typedef enum {
    RouletteWin,
    RouletteLose,
    TimeGate,
    Continue
} TerminationResult;

///////////////////////////////////////////////////////////////////
// Helper definitions
void scatter(
    Packet* restrict            pkt,
    const Material* restrict    mat,
    const float                 rn1,
    const float                 rn2    
);

void spin_packet(
    Packet* restrict            pkt,
    const float                 costheta,
    const float                 sintheta,
    const float                 cosphi,
    const float                 sinphi
);
///////////////////////////////////////////////////////////////////

#define ACCUM_WRITE_DELAY 15

/**
 * DROPSPIN kernel
 *
 * @param wmin minimum weight of packet before going to roulette
 * @param prwin probability of a roulette win for a packet with weight < wmin
 * @param materials_count the number of materials in the model
 * @param tetras_count the number of tetras in the model
 * @param materials_mem the pointer to the list of materials.
 * @param tetraAbsorb_mem the absorption accumulator memory for the tetras
 */
__kernel void drop_spin_kernel(
    const float                         wmin,
    const float                         prwin,
    const uint                          materials_count,
    const uint                          tetras_count,
    __constant Material* restrict       materials_mem,
    __global long* restrict             tetraAbsorb_mem
)
{
    // Setup local accumulation arrays
    local Material materials_cache[MAX_FPGACL_MATERIALS];
    local long tetraAbsorb_cache[MAX_FPGACL_ONCHIP_TETRAS];

    for(int i = 0; i < materials_count; i++) {
        materials_cache[i] = materials_mem[i];
    }
    for(int i = 0; i < MAX_FPGACL_ONCHIP_TETRAS; i++) {
        tetraAbsorb_cache[i] = 0;
    }

    // shift register cache
    uint sr_cache_tag[ACCUM_WRITE_DELAY+1];
    long sr_cache_val[ACCUM_WRITE_DELAY+1];

    #pragma unroll
    for(int i = 0; i < ACCUM_WRITE_DELAY+1; i++) {
        sr_cache_tag[i] = 0;
        sr_cache_val[i] = 0;    
    }

    #pragma ivdep safelen(ACCUM_WRITE_DELAY)
    while(true) {
        // read packet from HOP kernel
        bool valid = false;
        bool dead = false;
        bool sim_done = false;

        // check if EXIT kernel has finished and the simulation is done
        (void) read_channel_nb_intel(EXIT_TO_DROPSPIN, &sim_done);
        if(sim_done) {
            break;
        }

        ChannelData chanData = read_channel_nb_intel(INTERFACE_TO_DROPSPIN, &valid);

        if(valid) {
            if(chanData.state == DROPSPIN) {
                Packet pkt = chanData.pkt;
                Tetra currTetra = chanData.tetra;
                Material currMat = materials_cache[currTetra.matID];
                
                //// ACCUMULATION ////
                const double pktWeightLoss = pkt.w * currMat.absfrac;
                const long pktWeightLossFixed = DOUBLE2FIXED(pktWeightLoss);

                // remove the weight from the packet
                pkt.w -= pktWeightLoss;

                // get current absorbed energy in tetra
                long currVal = tetraAbsorb_cache[pkt.tetraID];

                // if there is a write in-flight (in shift register cache), use that
                #pragma unroll
                for (int i = 0; i < ACCUM_WRITE_DELAY + 1; i++) {
                    if (sr_cache_tag[i] == pkt.tetraID) {
                        currVal = sr_cache_val[i];
                    }
                }

                // keep track current writes in flight (shift register)
                #pragma unroll
                for (int i = 0; i < ACCUM_WRITE_DELAY; i++) {
                    sr_cache_tag[i] = sr_cache_tag[i+1];
                    sr_cache_val[i] = sr_cache_val[i+1];
                }

                // start the next write to absorb cache and into shift register at the same time
                const long newVal = __fpga_reg(currVal + pktWeightLossFixed);
                sr_cache_val[ACCUM_WRITE_DELAY] = tetraAbsorb_cache[pkt.tetraID] = newVal;
                sr_cache_tag[ACCUM_WRITE_DELAY] = pkt.tetraID;

                //// ROULETTE ////
                // read 3 random numbers from the channel
                // could use anywhere between none and all three of these
                const float3 rnx3 = read_channel_intel(RNG_TO_DROPSPIN);
                const float rn0 = rnx3[0], rn1 = rnx3[1], rn2 = rnx3[2];

                // checking termination
                TerminationResult term;

                if(pkt.w < wmin) {
                    if(rn0 < prwin) {
                        // wins roulette, increase the energy of the packet by the chance it had of winning
                        pkt.w /= prwin;
                        term = RouletteWin;
                        dead = false;
                    } else {
                        // packet lost roulette - time to die
                        term = RouletteLose;
                        dead = true;
                    }
                } else {
                    // not time for roulette yet
                    term = Continue;
                    dead = false;
                }

                // does the packet live or die?
                if(term == Continue || term == RouletteWin) {
                    //// SPIN ////
                    scatter(
                        &pkt,
                        &currMat,
                        rn1,
                        rn2
                    );

                    // move to DRAWSTEP
                    ChannelData toDrawStep = {
                        .pkt = pkt,
                        // stepResult = DC
                        .tetra = currTetra,
                        .state = DRAWSTEP
                    };
                    
                    chanData = toDrawStep;
                }
            }
            
            // move the packet to the next kernel
            if(dead) {
                write_channel_intel(DROPSPIN_TO_EXIT, 1);
            } else {
                write_channel_intel(DROPSPIN_TO_DRAWSTEP, chanData);
            }
        }
    }

    // when the simulation is done, copy the on-chip accumulation caches back to global memory
    for(int i = 0; i < MAX_FPGACL_ONCHIP_TETRAS; i++) {
        tetraAbsorb_mem[i] = tetraAbsorb_cache[i];
    }
}

/**
 * Given a spin matrix [[costheta, sintheta], [cosphi, sinphi]) 'spins' the current packet.
 *
 * @param pkt the packet
 * @param costheta spin matrix [0][0]
 * @param sintheta spin matrix [0][1]
 * @param cosphi spin matrix [1][0]
 * @param sinphi spin matrix [1][1]
 */
void spin_packet(
    Packet* restrict            pkt,
    const float                 costheta,
    const float                 sintheta,
    const float                 cosphi,
    const float                 sinphi
)
{
    // the current direction vectors
    const float3 d0 = pkt->dir.d;
    const float3 a0 = pkt->dir.a;
    const float3 b0 = pkt->dir.b;

    // update the direction vectors
    pkt->dir.d = costheta*d0 - sintheta*cosphi*a0 + sintheta*sinphi*b0;
    pkt->dir.a = sintheta*d0 + costheta*cosphi*a0 - costheta*sinphi*b0;
    pkt->dir.b = sinphi*a0 + cosphi*b0;
}

/**
 * Scatters (spin) a packet based on the tetra it is currently in
 *
 * @param pkt the packet to be scattered
 * @param mat the material of the tetra the packet is in
 * @param rn1 one of two random numbers for this function U[0,1)
 * @param rn2 one of two random numbers for this function U[0,1)
 */
void scatter(
    Packet* restrict            pkt,
    const Material* restrict    mat,
    const float                 rn1,
    const float                 rn2    
)
{
    const float phi = 2.0f * M_PI * rn1;  // U[0,2PI)
    const float q = 2.0f * rn2 - 1.0f; // U[-1,1)

    // generate the spin matrix
    float costheta;

    if(mat->isotropic) {  // isotropic --> g ~= 0.0
        costheta = q;
    } else {
        // t = (1-g*g)/(1.0 + g*q)
        const float t = mat->one_minus_gg / (1.0f + mat->g*q);

        // costheta = 1/(2*g) * (1 + g*g - t*t)
        costheta = clamp(mat->recip_2g * (mat->one_plus_gg - t*t), -1.0f, 1.0f);  // clamp this to [-1,1]
    }

    const float sintheta = sqrt(1-(costheta*costheta));
    const float cosphi = cos(phi);
    const float sinphi = sin(phi);

    // spin the packet
    spin_packet(
        pkt,
        costheta,
        sintheta,
        cosphi,
        sinphi
    );
}


