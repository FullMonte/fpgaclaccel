#include "channels.h"
#include "FullMonteFPGACLConstants.h"

// TODO: tons of opportunity to reduce resource utilization in here!

// Mersenne twister constants
#define MT_M                397
#define MT_N                624
#define MATRIX_A            0x9908b0dfUL
#define UPPER_MASK          0x80000000UL
#define LOWER_MASK          0x7fffffffUL

// Used to ensure that the uniformly generated random numbers are in the range (0,1)
#define CLAMP_ZERO          0x1.0p-126f
#define CLAMP_ONE           0x1.fffffep-1f

// The RNG will create 20 random numbers per clock cycle
// (PIPELINES*random numbers per pipeline = 4*5 = 20) this can feed up 
// to 4 FullMonte pipelines
#define RNS_PER_PIPELINE    5
#define VECTOR              64
#define VECTOR_DIV2         VECTOR/2
#define VECTOR_DIV4         VECTOR/4

// internal channels
// TODO: is 8 required ???
channel uint16 INIT_STREAM_0 __attribute__((depth(8))); 
channel uint16 INIT_STREAM_1 __attribute__((depth(8))); 
channel uint16 INIT_STREAM_2 __attribute__((depth(8))); 
channel uint16 INIT_STREAM_3 __attribute__((depth(8)));

// initialize the mersenne twister
// The code below is slightly complicated because we wish to produce 64 values at a time;
// however, the mersenne twister state has 624 values. This is not evenly divisible by 64
// so there are some initial values that are writtent to the channel which are never used
__kernel void rng_init_kernel(const unsigned int seed) {
    unsigned int state = seed;
    uint ival[VECTOR];
    
    // initialize with seed
    #pragma unroll VECTOR
    for (int i=0; i<VECTOR; i++) {
       ival[i] = seed;
    }
    
    // initialize loop
    for (unsigned int n=0; n<MT_N; n++) {
        // ival is a shift register, this performs the shift
        #pragma unroll
        for (int i=0; i<VECTOR-1; i++) {
            ival[i] = ival[i+1];
        }

        ival[VECTOR-1] = state;
        state = (1812433253U * (state ^ (state >> 30)) + n) & 0xffffffffUL;
        if ((n & (VECTOR-1)) == 47) {
            uint16 I0, I1, I2, I3;
            #pragma unroll VECTOR_DIV4
            for (int i=0; i<VECTOR_DIV4; i++) {
                I0[i]=ival[i];
                I1[i]=ival[i+1*VECTOR_DIV4];
                I2[i]=ival[i+2*VECTOR_DIV4];
                I3[i]=ival[i+3*VECTOR_DIV4];
            }

            // init to generator channels
            write_channel_intel(INIT_STREAM_0, I0);
            write_channel_intel(INIT_STREAM_1, I1);
            write_channel_intel(INIT_STREAM_2, I2);
            write_channel_intel(INIT_STREAM_3, I3);
        }
    }
}

// This kernel implements the mersenne twister random number generator. 
// It is almost a direct implementation of the algorithm shown
// here: http://en.wikipedia.org/wiki/Mersenne_twister
__kernel void rng_kernel(void) {
    unsigned int mt[MT_N];

    bool read_from_initialization = true;
    ushort num_initializers_read = 0;

    while(true) {
        uint y[VECTOR];      

        bool write_channel = false;
        if (read_from_initialization) {
            // INITIALIZING
            // read from initialization channel
            uint16 I0 = read_channel_intel(INIT_STREAM_0);
            uint16 I1 = read_channel_intel(INIT_STREAM_1);
            uint16 I2 = read_channel_intel(INIT_STREAM_2);
            uint16 I3 = read_channel_intel(INIT_STREAM_3);
            
            #pragma unroll VECTOR_DIV4
            for (int i=0; i<VECTOR_DIV4; i++) {
                y[i]=I0[i];
                y[i+1*VECTOR_DIV4]=I1[i];
                y[i+2*VECTOR_DIV4]=I2[i];
                y[i+3*VECTOR_DIV4]=I3[i];
            }
            
            if (++num_initializers_read == MT_N/VECTOR+1) {
                // TODO: never get here...
                printf("DEVICE (RNG): Done initialization\n");
                read_from_initialization=false;
            }
        } else {
            // NOT INITIALIZING
            #pragma unroll VECTOR
            for (int i=0; i<VECTOR; i++) {
                y[i] = (mt[i]&UPPER_MASK)|(mt[i+1]&LOWER_MASK);
                y[i] = mt[i+MT_M] ^ (y[i] >> 1) ^ (y[i] & 0x1UL ? MATRIX_A : 0x0UL);
            }
            write_channel = true;
        }

        #pragma unroll 
        for (int i=0; i<MT_N-VECTOR; i++) {
             mt[i]=mt[i+VECTOR];
        }

        float U[VECTOR];
        #pragma unroll VECTOR
        for (int i=0; i<VECTOR; i++) {
            mt[MT_N-VECTOR+i]=y[i];

            // Tempering
            y[i] ^= (y[i] >> 11);
            y[i] ^= (y[i] << 7) & 0x9d2c5680UL;
            y[i] ^= (y[i] << 15) & 0xefc60000UL;
            y[i] ^= (y[i] >> 18);

            U[i] = (float)y[i] / 4294967296.0f;

            // clamping to [0,1)
            //if (U[i] == 0.0f) U[i] = CLAMP_ZERO; // uncomment to clamp to (0,1)
            if (U[i] == 1.0f) U[i] = CLAMP_ONE;
        }

        // converting to correct format for FullMonte and writing to output channels
        if (write_channel) {
            // at this point 'U' is an array of 64 floating point random numbers U[0,1).
            // we can do with them as we please
            const float3 dropspin_data = {U[0], U[1], U[2]};
            const float interface_data = U[3];
            const float drawstep_data = U[4];

            // TODO: tons of extra random numbers here for extra pipelines
            // TODO: too many extra random numbers, can we reduce resource utilization?
            
            // write to output channels
            // use non-blocking here so we don't cause pipeline deadlock
            // if we produce extra random numbers they are simply dropped
            write_channel_nb_intel(RNG_TO_DROPSPIN, dropspin_data);
            write_channel_nb_intel(RNG_TO_INTERFACE, interface_data);
            write_channel_nb_intel(RNG_TO_DRAWSTEP, drawstep_data);
        }
    }
}

