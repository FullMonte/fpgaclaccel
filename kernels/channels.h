#ifndef _CHANNELS_H_
#define _CHANNELS_H_

#include "FullMonteFPGACLTypes.h"

// enable channels
#pragma OPENCL EXTENSION cl_intel_channels : enable

/**
 * Internal state each packet is marked with to determine
 * which stage of the pipeline should be run on it next
 */
typedef enum {
    DRAWSTEP,
    HOP,
    INTERFACE,
    DROPSPIN
} KernelState;

/**
 * Combination of data to be passed between kernels
 * This is a superset of data passing (i.e. not all of the 
 * data will be used in each kernel)
 */
typedef struct {
    Packet          pkt;
    StepResult      stepResult;
    Tetra           tetra;
    KernelState     state;
    bool            exit;
} ChannelData;

////////////////////////////////////////////////////////////////////////////////
////// Channels to move packets between kernels
// kernel-kernel channels
channel ChannelData             LAUNCH_TO_DRAWSTEP      __attribute__((depth(1)));

channel ChannelData             DRAWSTEP_TO_HOP         __attribute__((depth(1)));
channel ChannelData             HOP_TO_INTERFACE        __attribute__((depth(1)));
channel ChannelData             INTERFACE_TO_DROPSPIN   __attribute__((depth(1)));
channel ChannelData             DROPSPIN_TO_DRAWSTEP    __attribute__((depth(1)));

channel int                     INTERFACE_TO_EXIT       __attribute__((depth(1)));
channel int                     DROPSPIN_TO_EXIT        __attribute__((depth(1)));
channel bool                    EXIT_TO_DROPSPIN        __attribute__((depth(1)));

// rng-kernel channels
channel float3                  RNG_TO_DROPSPIN         __attribute__((depth(1)));
channel float                   RNG_TO_DRAWSTEP         __attribute__((depth(1)));
channel float                   RNG_TO_INTERFACE        __attribute__((depth(1)));
////////////////////////////////////////////////////////////////////////////////

#endif

