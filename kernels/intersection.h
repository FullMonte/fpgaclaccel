#ifndef _INTERSECTION_H
#define _INTERSECTION_H

#include "FullMonteFPGACLTypes.h"


/**
 * Calculates if a given photon with position p and direction d intersects
 * with a face of the current tetra within a random step length (pkt.s).
 * This function is in its own header file because it is shared by
 * two kernels: draw_step.cl and interface.cl
 *
 * @param T the tetra
 * @param pkt the packet
 * @returns the StepResult
 */
StepResult getIntersection(const Tetra *T, const Packet *pkt) {
    StepResult result;
    result.idx = -1;

    //
    // compute n (dot) d for all faces of T
    //      dot[face] = n[face] (dot) packet direction
    //
    // also calculate constraint if point lies within the tetra.
    // Which also represents the height which the photon position
    // is over the face of the respective Tetra
    //      h1[face] = C - n[face] (dot) packet position
    //
    float4 dots;
    float4 h1;
    #pragma unroll 4
    for(int f = 0; f < 4; f++) {
        // the normal of face 'f'
        const float3 n = {T->nx[f], T->ny[f], T->nz[f]};
        
        dots[f] = dot(n, pkt->dir.d);
        h1[f] = T->C[f] - dot(n, pkt->p);
    }

    //
    // dist is the distance of the photon to the 4 faces of the tetra.
    // After distance is found, check if dot is positive or negative. If
    // it is negative use the value of dist, otherwise use infinity.
    //
    float4 dist;
    #pragma unroll 4
    for(int f = 0; f < 4; f++) {
        if(dots[f] < 0) {
            dist[f] = h1[f] / dots[f];
        } else {
            dist[f] = FLT_MAX;
        }
    }

    //
    // at most 3 points of the dot products should be negative:
    // height  dot     h/dot   meaning
    //  +       +       +       OK: inside, facing away (no possible intersection)
    //  +       -       -       OK: inside, facing towards (intersection possible)
    //  -       +       -       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
    //  -       -       +       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)
    //
    // require n dot p - C > 0 (above face) and d dot n < 0
    // Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
    //
    uint min_idx = 0;
    float min_idx_val = FLT_MAX;

    for(int f = 0; f < 4; f++) {
        if(dist[f] < min_idx_val) {
            min_idx_val = dist[f];
            min_idx = f;
        }
    }

    //
    // Is the smallest distance of the photon smaller than the random step length s?
    // If yes, return true. This means that the photon intersects with one of the tetras
    // faces within the random step length
    //
    // if min_idx_val[0] < s[0]
    //
    result.hit = min_idx_val < pkt->s[0];

    //
    // Choose the face with the smallest distance to the photons position and store it in
    // IDfe. Bitwise AND of the index in min_idx because the index can also have the 
    // value 4 which would result in bad memory access (size of IDfds is 4 [0-3]
    //
    result.IDfe = T->IDfds[min_idx&3];

    //
    // Choose the adjacent tetra of the nearest face to the photons position and store it in
    // IDte. Bitwise AND of the index in min_idx because the index can also have the 
    // value 4 which would result in bad memory access (size of IDte is 4 [0-3])
    //
    result.IDte = T->adjTetras[min_idx&3];

    // will be 4 if no min found
    result.idx = min_idx;

    // compare the smallest distance min_idx_val with the random step length
    // and store the smaller value in result.distance
    result.distance = fmin(min_idx_val, pkt->s[0]);
    result.distance = fmax(result.distance, 0.0f);

    //
    // Determine the new position of the photon after the step length.
    //
    result.Pe = pkt->p + (pkt->dir.d * result.distance);

    return result;
}

#endif

