#include "channels.h"
#include "intersection.h"

#include "FullMonteFPGACLTypes.h"

/**
 * DRAWSTEP kernel
 *
 * @param materials_count the number of materials.
 * @param tetras_count the number of tetras.
 * @param materials_mem the pointer to the list of materials.
 * @param tetras_mem the pointer to the list of tetras.
 */
__kernel void draw_step_kernel(
    const uint                          materials_count,
    const uint                          tetras_count,
    __constant Material* restrict       materials_mem,
    __global const Tetra* restrict      tetras_mem
)
{
    // setup local caches for materials and tetras
    local Material materials_cache[MAX_FPGACL_MATERIALS];
    local Tetra tetras_cache[MAX_FPGACL_ONCHIP_TETRAS];
    
    for(int i = 0; i < materials_count; i++) {
        materials_cache[i] = materials_mem[i];
    }

    for(int i = 0; i < tetras_count; i++) {
        tetras_cache[i] = tetras_mem[i];
    }

    while(true) {
        bool valid_launch = false, valid_dropspin = false;

        // first try reading from dropspin
        ChannelData chanData = read_channel_nb_intel(DROPSPIN_TO_DRAWSTEP, &valid_dropspin);

        // give priority to dropspin, if nothing, try launch
        if(!valid_dropspin) {
            // read a launch packet from the channel
            chanData = read_channel_nb_intel(LAUNCH_TO_DRAWSTEP, &valid_launch);
        }

        if(valid_dropspin || valid_launch) {
            if(chanData.state == DRAWSTEP) {
                Packet pkt = chanData.pkt;

                // get the current tetra and the material of that tetra
                const Tetra T = tetras_cache[pkt.tetraID];
                const Material M = materials_cache[T.matID];
            
                // draw step length
                // 1.0 - [0,1) = [1.0, 0) to avoid 0 for log(x)
                const float rn = read_channel_intel(RNG_TO_DRAWSTEP);  // U[0,1)
                const float floatExpRn = -log(1.0f - rn);
            
                // initial propagation vector
                // (1/muT physical step remaining, 1 dimensionless step remaining, 0 time)
                pkt.s = floatExpRn * M.m_init;

                // create the package of Packet, Tetra and StepResult to send to hop kernel
                ChannelData hopData = {
                    .pkt = pkt,
                    .stepResult = chanData.stepResult,
                    .tetra = T,
                    .state = HOP
                };

                chanData = hopData;
            }
        
            write_channel_intel(DRAWSTEP_TO_HOP, chanData);
        }
    }
}

