#include "channels.h"
#include "FullMonteFPGACLConstants.h"

// enable Channels
#pragma OPENCL EXTENSION cl_intel_channels : enable

// enable 64-bit computation for doubles
#pragma OPENCL EXTENSION cl_khr_fp64 : enable 

/////////////////////////////////////
//// kernels that make up the pipline
#include "rng.cl"
#include "launch.cl"
#include "draw_step.cl"
#include "hop.cl"
#include "interface.cl"
#include "drop_spin.cl"
#include "exit.cl"
/////////////////////////////////////

