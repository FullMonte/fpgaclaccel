#include "channels.h"
#include "intersection.h"

#include "FullMonteFPGACLTypes.h"

///////////////////////////////////////////////////////////////////
// Helper definitions
bool reflect_refract(
    Packet* restrict            pkt,
    const Tetra* restrict       currTetra,
    const StepResult* restrict  stepResult,
    const float                 ni,
    const float                 nt,
    uint* restrict              IDt_next
);
///////////////////////////////////////////////////////////////////

/**
 * INTERFACE kernel
 *
 * @param materials_count the number of materials.
 * @param tetras_count the number of tetras.
 * @param materials_mem the pointer to the list of materials.
 * @param tetras_mem the pointer to the list of tetras.
 */
__kernel void interface_kernel(
    const uint                          materials_count,
    const uint                          tetras_count,
    __constant Material* restrict       materials_mem,
    __global const Tetra* restrict      tetras_mem
)
{
    // setup local caches for materials and tetras
    local Material materials_cache[MAX_FPGACL_MATERIALS];
    local Tetra tetras_cache[MAX_FPGACL_ONCHIP_TETRAS];
    
    for(int i = 0; i < materials_count; i++) {
        materials_cache[i] = materials_mem[i];
    }

    for(int i = 0; i < tetras_count; i++) {
        tetras_cache[i] = tetras_mem[i];
    }

    while(true) {
        bool valid = false;
        bool exiting = false;
        ChannelData chanData = read_channel_nb_intel(HOP_TO_INTERFACE, &valid);
        
        if(valid) {
            if(chanData.state == INTERFACE) {
                // unpack data from channel
                Packet pkt = chanData.pkt;
                StepResult stepResult = chanData.stepResult;
                Tetra currTetra = chanData.tetra;
                uint IDt = pkt.tetraID;

                // locals
                uint IDt_next = IDt;
                const Tetra nextTetra = tetras_cache[stepResult.IDte];

                const uint IDm = currTetra.matID;
                uint IDm_next = IDm;
                const uint IDm_bound = nextTetra.matID;
                const Material currMat = materials_cache[IDm];

                // update the step length
                pkt.s = pkt.s + (stepResult.distance * currMat.m_prop);

                if(IDm == IDm_bound) {  // not changing materials
                    IDt_next = stepResult.IDte;
                } else {                // material change
                    const float nt = materials_cache[IDm_bound].n;
                    const float ni = currMat.n;

                    // reflection/reflaction if refractive indices are different
                    if(ni == nt) {
                        // move to the next tetra
                        IDt_next = stepResult.IDte;
                    } else {
                        //// REFLECT/REFRACT ////
                        // this function may modify 'pkt' and 'IDt_next'
                        reflect_refract(
                            &pkt,
                            &currTetra,
                            &stepResult,
                            ni,
                            nt,
                            &IDt_next
                        );
                    }
                }

                // check for tetra change
                if(IDt != IDt_next) {
                    if(IDt_next == 0) {
                        //// EXITING MESH ////
                        exiting = true;
                    } else {
                        //// NEW TETRA ////
                        exiting = false;
                        IDt = IDt_next;
                        IDm_next = IDm_bound;
                        currTetra = nextTetra;  // tetra_mem[IDt] == tettras_mem[IDt_next]
                    }
                } else {
                    exiting = false;
                }
                
                if(!exiting) {
                    //// NOT EXITING MESH ////
                    // check if we changed materials
                    if(IDm != IDm_next) {
                        const Material nextMat = materials_cache[IDm_next];

                        // acount for material change in packet step
                        pkt.s[0] = pkt.s[1] * nextMat.recip_mu_t;
                        pkt.s[2] = 0.0f;
                    }

                    // update the local packet tetra ID (DO THIS BEFORE getIntersection call)
                    // NOTE: this is different than software and CUDA versions
                    pkt.tetraID = IDt;

                    //// SENDIND BACK TO HOP ////
                    const ChannelData toHop = {
                        .pkt = pkt,
                        // stepResult = Do not care
                        .tetra = currTetra,
                        .state = HOP
                    };

                    chanData = toHop;
                }
            }

            // move to next state
            if(exiting) {
                write_channel_intel(INTERFACE_TO_EXIT, 1);
            } else {
                write_channel_intel(INTERFACE_TO_DROPSPIN, chanData);
            }
        }
    }
}


/**
 * Perform reflection and refraction when the intersection occurs between tetras
 * with materials of different refractive indices ('n'). The packet may either 
 * reﬂect or have its angle to the normal refract. Packet is moving from a material
 * with refractive index 'ni' to 'nt'.
 *
 * @param pkt the packet to be reflected/refracted
 * @param currTetra the Tetra the packet is currently in (TODO: is this before or after step?)
 * @param stepResult the result of the step
 * @param ni the refractive index of the tetra the packet is moving from
 * @param nt the refractive index of the tetra the packet is moving to
 * @param IDt_next the ID of the next tetra (output)
 */
bool reflect_refract(
    Packet* restrict            pkt,
    const Tetra* restrict       currTetra,
    const StepResult* restrict  stepResult,
    const float                 ni,
    const float                 nt,
    uint* restrict              IDt_next
)
{
    // the normal of the face closest to the packet
    const float3 normal = {
        currTetra->nx[stepResult->idx],
        currTetra->ny[stepResult->idx],
        currTetra->nz[stepResult->idx]
    }; 
    const float ni_nt_ratio = ni/nt;

    const float cosi = fmin(1.0f, -dot(normal, pkt->dir.d));
    const float sini = sqrt(1.0f-(cosi*cosi));
    const float sint = ni_nt_ratio * sini;
    const float cost = sqrt(1.0f-(sint*sint));

    bool ret = false;

    // this will be set in the if/else block below, it will be the
    // new direction ('d' vector) of the packet.
    float3 newd;

    // Check for Total Internal Reflection? (TIR)
    if(1.0f < sint) {
        // reflection:
        //      d'  = d + 2*(d (dot) n) * n 
        //          = d + 2*cosi*n
        newd = pkt->dir.d + 2.0f*cosi*normal;
    } else {
        const float3 d_p = pkt->dir.d + cosi * normal;

        const float root_Rs = (ni*cosi - nt*cost) / (ni*cosi + nt*cost);
        const float root_Rp = (ni*cost - nt*cosi) / (ni*cost + nt*cosi);
        const float pr = 0.5f * (root_Rs*root_Rs + root_Rp*root_Rp);

        // get random number for fresnel reflection
        const float frn = read_channel_intel(RNG_TO_INTERFACE); // U[0,1)

        // do fresnel reflection probabilisticly
        if(frn < pr) {
            // reflect
            newd = pkt->dir.d + 2.0f*cosi*normal;
        } else {
            // fresnel reflection
            newd = ni_nt_ratio*d_p - cost*normal;
            *IDt_next = stepResult->IDte;
        }
    }

    // create a normal by zeroing smallest element, transposing two elements and normalizing
    // e.g. [dx, dy, dz] => [-dy, dx, 0] if |dz| < {|dy|, |dx|}
    // TODO: could do better and avoid loop and mod operators
    uint m = 0, i, j;
    float min_val = fabs(newd[0]);
    for(int t = 1; t < 3; t++) {
        if(fabs(newd[t]) < min_val) {
            m = t;
            min_val = fabs(newd[t]);
        }
    }
    i = (m+1)%3;
    j = (m+2)%3;

    const float k = 1.0f/sqrt(newd[i]*newd[i] + newd[j]*newd[j]);

    // change the direction of the packet
    pkt->dir.d = newd;

    // update 'a' and 'b' vectors based on the new packet direction 'd'
    pkt->dir.a[m] = 0.0f;
    pkt->dir.a[i] = newd[j] * k;
    pkt->dir.a[j] = -newd[i] * k;

    pkt->dir.b = cross(newd, pkt->dir.a);

    return ret;
}

