#pragma once

#include "FullMonteFPGACLTypes.h"

#include <CL/opencl.h>
#include <AOCLUtils/aocl_utils.h>

#include <vector>

using namespace std;
    
namespace FPGACL {

bool init(
    const string                        aocx_binary_file,
    const unsigned                      seed,
    const float                         wmin,
    const float                         prwin,
    const vector<FPGACL::Material>&     materials,
    const vector<FPGACL::Tetra>&        tetras
);
bool launch(const vector<FPGACL::LaunchPacket>& launched_packets);
void sync();
void destroy();

void copyTetraEnergy(vector<double>& tetraEnergy);
void copyTetraEnergy(vector<float>& tetraEnergy);

}  // END: FPGACL namespace

// call back for the Intel FPGA OpenCL runtime
// cannot be inside a namespace
void cleanup();

