#ifndef _FULLMONTEFPGACLCONSTANTS_H_
#define _FULLMONTEFPGACLCONSTANTS_H_

// the speed of light in mm/ns
#define SPEED_OF_LIGHT_MM_PER_NS                299.792458f

// max number of materials
#define MAX_FPGACL_MATERIALS                    16

// max tetras cached on-chip
#define MAX_FPGACL_ONCHIP_TETRAS                65536

// maximum number of FullMonte pipelines on a single device
#define MAX_FPGACL_PIPELINES                    4

// accumulation fixed point bits 28.36 format
#define FIXED_POINT_INTEGRAL_BITS               28
#define FIXED_POINT_FRACTIONAL_BITS             36

// macros to convert between fixed and floating for tetra absorb
#define DOUBLE2FIXED(x)                         ((long)((x) * ((long)(1) << FIXED_POINT_FRACTIONAL_BITS)))
#define FIXED2DOUBLE(x)                         (((double)(x)) / ((long)(1) << FIXED_POINT_FRACTIONAL_BITS))

#endif 


